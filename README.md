Forked OpenCV 2.4 for Iris Recogntion.

To build in Linux:

1. Make sure you are in the root of the project directory.
2. Generate build files.

	mkdir build && cd build && cmake ..

3. Begin the build process.

	make

To build in Windows:

1. Make sure you are in the root of the project directory in command prompt.
2. Generate build files.

	mkdir build
	cd build
	cmake ..

3. Begin the build process.

	Open OpenCV.sln in Visual Studio.

	Right click on the ALL_BUILD project at the bottom of the solution
	explorer and select Build.

Refer to http://docs.opencv.org/doc/tutorials/introduction/windows_install/windows_install.html
