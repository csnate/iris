#include <iostream>

#include "opencv2/iris/iris.hpp"
#include "opencv2/highgui/highgui.hpp"

using namespace cv;
using namespace std;

int main()
{
	Mat img1 = imread("iris2.jpg");
	Mat img2 = imread("iris2.jpg");

	Iris foo(img1);
	Iris bar(img2);

	cout << foo.compare(bar) << endl;
	
	return 0;
}
