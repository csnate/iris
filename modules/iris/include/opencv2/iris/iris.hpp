#ifndef IRIS_H
#define IRIS_H

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/imgproc_c.h"

#include <stdexcept>
#include <vector>

class CV_EXPORTS Iris
{
public:

	/**
	 * Construct an Iris object with a image matrix.  Expects a full sized
	 * image of an eye.
	 */
	Iris(const cv::Mat&);

	/**
	 * Compare another Iris returning a match value normalized from 0 to 1,
	 * 0 = no match, 1 = 100% match.
	 */
	double compare(const Iris&) const;

	cv::Mat tape_image;

	/**
	 * Transform an image containing an iris into Log-Polar coordinates for
	 * processing.
	 */
	const cv::Mat& transformIris(const cv::Mat& in, cv::Mat& out);

	/**
	 * Given the vector representation of a pupil and input image, extract
	 * the iris from the input.
	 */
	const cv::Mat& extractIris(const cv::Mat& in, const cv::Vec3i& pupil,
		cv::Mat& out);

	/**
	 * Locates the pupil within a the matrix of an image containing an eye.
	 * The pupil attributes are returned as a vector containing both the
	 * location of the pupil its radius.  The image is assumed to contain
	 * a human eye and also be grayscale.
	 *
	 * See here for basic implementation:
	 * http://opencv-code.com/tutorials/pupil-detection-from-an-eye-image/
	 */
	const cv::Vec3i& findPupil(const cv::Mat& image, cv::Vec3i& pupilAttrs);
};

#endif
