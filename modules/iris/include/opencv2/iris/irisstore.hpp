#include "opencv2/iris/iris.hpp"
#include <vector>

#ifndef IRIS_STORE_H
#define IRIS_STORE_H

class CV_EXPORTS IrisStore
{
	private:
		std::vector<Iris> registered_irides;

	public:
		IrisStore();
		const std::vector<Iris>& findMatches(const Iris& test_iris, std::vector<Iris>& output) const;
		void registerIris(const Iris& nIris);
};


#endif
