#include "opencv2/iris/iris.hpp"

#include <iostream>

using namespace cv;
using namespace std;

Iris::Iris(const Mat& img)
{
	Mat gray;
	cvtColor(img, gray, CV_BGR2GRAY);

	Vec3i pupil;
	findPupil(gray, pupil);

	Mat iris;
	extractIris(gray, pupil, iris);

	tape_image = Mat(gray.size(), gray.type());
	transformIris(iris, tape_image);
}

double Iris::compare(const Iris& other) const
{
	int minHessian = 1;

	SurfFeatureDetector detector(minHessian);

	vector<KeyPoint> keypoints_1, keypoints_2;

	detector.detect(this->tape_image, keypoints_1);
	detector.detect(other.tape_image, keypoints_2);

	SurfDescriptorExtractor extractor;
	
	Mat descriptors_1, descriptors_2;

	extractor.compute(this->tape_image, keypoints_1, descriptors_1);
	extractor.compute(other.tape_image, keypoints_2, descriptors_2);

	FlannBasedMatcher matcher;
	vector<DMatch> matches;

	matcher.match(descriptors_1, descriptors_2, matches);

	double max_dist = 0.0;
	double min_dist = 0.2;

	for(int i = 0; i < descriptors_1.rows; ++i)
	{
		double dist = matches[i].distance;
		if(dist < min_dist) min_dist = dist;
		if(dist > max_dist) max_dist = dist;
	}

	int good_matches = 0;

	for(int i = 0; i < descriptors_1.rows; ++i)
	{
		if(matches[i].distance <= max(2*min_dist, 0.02))
			++good_matches;
	}


	return ((double) good_matches) / ((double) matches.size());
}

const Vec3i& Iris::findPupil(const Mat& image, Vec3i& pupilAttrs)
{
	/// Copy the source image and invert it.
	Mat srcimg(~image);

	/// Convert the image to a binary image.
	threshold(srcimg, srcimg, 220, 255, THRESH_BINARY);

	/// Locate the contours within the image and fill the holes inside them.
	vector<vector<Point> > contours;
	findContours(srcimg, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
	drawContours(srcimg, contours, -1, CV_RGB(255, 255, 255), -1);

	bool pupilfound = false;

	/// Iterate over the contours in the image and locate the pupil.
	for(size_t i = 0; i < contours.size(); ++i)
	{
		double area = contourArea(contours[i]);
		Rect rect = boundingRect(contours[i]);
		int radius = rect.width / 2;

		/// If the current contour looks like a pupil, set the return
		/// values and end the loop.
		if(area >= 30 &&
			abs(1.0 - ((double) rect.width / rect.height)) <= 0.2 &&
			abs(1.0 - (area / (CV_PI * pow(radius, 2)))) <= 0.2)
		{
			pupilfound = true;

			pupilAttrs(0) = rect.x + radius;
			pupilAttrs(1) = rect.y + radius;
			pupilAttrs(2) = radius;

			i = contours.size();
		}
	}

	if(!pupilfound)
		throw runtime_error("Pupil not found.");

	return pupilAttrs;
}

const Mat& Iris::transformIris(const Mat& in, Mat& out)
{
	/// We will need to use some of the C interface functionality, so
	/// there will be some conversions here.
	CvMat cvin = in;
	CvMat cvres = out;
	cvLogPolar(&cvin, &cvres,
		Point(in.size().width / 2.0, in.size().height / 2.0), 60.0);

	/// Crop the transformed iris.
	Mat thresh;
	threshold(out, thresh, 1, 255, THRESH_BINARY);
	vector<vector<Point> > points;
	findContours(thresh, points, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
	Rect roi = boundingRect(points[0]);
	out = out(roi);

	return out;
}

const Mat& Iris::extractIris(const Mat& in, const Vec3i& pupil, Mat& out)
{
	/// To extract the iris, I simply double the radius of the pupil and
	/// use that as a boundary for extracting the iris.  There is probably
	/// a better way to do this, but for the time being it does work with
	/// our test data set.
	Vec3i inner_boundary = pupil;
	Vec3i outer_boundary(pupil(0), pupil(1), pupil(2) * 2);

	/// Generate mask used for extraction.
	Mat mask(in.size(), CV_8UC1);
	mask.setTo(Scalar(0, 0, 0));
	circle(mask, Point(outer_boundary(0), outer_boundary(1)),
		outer_boundary(2), CV_RGB(255, 255, 255), -1, 8, 0);
	circle(mask, Point(inner_boundary(0), inner_boundary(1)),
		inner_boundary(2), CV_RGB(0, 0, 0), -1, 8, 0);

	/// Perform a bitwise 'and' on the input image with the generated mask
	/// to zero out any pixels that are not part of the iris.
	Mat masked;
	bitwise_and(in, mask, masked);

	/// Even though we have an image with only the iris, we will still need
	/// to crop as much as we can out of the image.
	Rect cropBox(outer_boundary(0) - outer_boundary(2),
		outer_boundary(1) - outer_boundary(2), outer_boundary(2) * 2,
		outer_boundary(2) * 2);
	out = masked(cropBox);

	return out;
}
