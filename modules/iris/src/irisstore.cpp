#include "opencv2/iris/irisstore.hpp"

using namespace std;

IrisStore::IrisStore() {}

const vector<Iris>& IrisStore::findMatches(const Iris& test_iris,
	vector<Iris>& output) const
{
	for(vector<Iris>::const_iterator it = registered_irides.begin();
		it != registered_irides.end(); ++it)
		if(it->compare(test_iris) >= 0.8)
			output.push_back(*it);

	return output;
}

void IrisStore::registerIris(const Iris& nIris)
{
	registered_irides.push_back(nIris);
}
